��          �   %   �      p     q     v  _   �     �  
   �     �               0  
   ?     J     S     \     e  	   y     �     �     �     �     �     �     �     �     �  
   �        7   "  �  Z     J     P  Q   _  	   �     �     �     �  "   �                     $     0     8     P     ]     o     v     �     �     �     �     �     �  	   �      �  7   �                      	                                                                                          
           1 %d 1 time %d times Adds a [download_page] shortcode for showing off your available downloads, tags and categories. Category Date added Download Download Monitor Download Monitor Page Addon Download count Downloaded Featured Filename Filesize No downloads found. Order by: Previous versions Search Search Downloads Search downloads... Searching for "%s" Subcategories: Tags Title Version Version %s https://www.download-monitor.com https://www.download-monitor.com/extensions/page-addon/ Project-Id-Version: Download Monitor Page Addon 1.1.6
Report-Msgid-Bugs-To: https://www.download-monitor.com/
POT-Creation-Date: 2015-07-01 12:43:28+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-11-27 13:44+0000
Last-Translator: PRZECINEK.com
Language-Team: Polish
X-Generator: Loco - https://localise.biz/
Language: pl-PL
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2) 1 %d  1 raz %d razy  Dodaje kod [download_page] aby wyświetlić dostępne pobrania, tagi i kategorie. Kategoria Dodano Pobierz Monitor Pobierania Dodatek Strony Monitora Pobierania Liczba pobrań Pobrano Wyróżniony Nazwa pliku Rozmiar Nie znaleziono plików. Kolejność: Poprzednie wersje Szukaj Szukaj plików Szukaj plików... Szukam "%s" Podkategorie: Tagi Tytuł Wersja Wersja %s https://www.download-monitor.com https://www.download-monitor.com/extensions/page-addon/ 