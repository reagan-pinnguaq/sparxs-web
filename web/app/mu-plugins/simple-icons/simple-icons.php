<?php

/*
Plugin Name:  Simple Icons
Plugin URI:   https://simpleicons.org/
Description:  A lightweight social icons plugin built on the simpleicons.org framework
Version:      1.1
Author:       Travis Taylor
Author URI:   https://webdesignyorkpa.com/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
*/

// For debugging purposes
// wp_cache_flush();

// function debug($data) {
// 	echo '<pre>';
// 	var_dump($data);
// 	echo '</pre>';
// }

function simpleicons_css() {
    ?>
        <style>
            span[class*="simple-icon-"] {
            	width: 1.5rem;
            	height: 1.5rem;
            	display: inline-block;

            }
            span[class*="simple-icon-"] svg {
            	display: inline-block;
            	vertical-align: middle;
                height: inherit;
                width: inherit;
            }
        </style>
    <?php
}
add_action('wp_head', 'simpleicons_css');


class SimpleIcons {
	static private $svgs = array();

	static public $placeholders = array();

	public static function register($data) {
		$slug = self::slugify_name($data->title);
		$data->svg = 'https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/' . $slug . '.svg';

		self::$svgs[$slug] = array(
			'data' => $data
		);

		self::$placeholders['#' . $slug . '#'] = array (
			'shortcode' => 'simple_icon',
            'atts' => array('name' => $slug, 'data' => $data),
            'content' => '',
		);
	}

	public static function display($data) {
		$data['name'] = self::slugify_name($data['name']);

		// vars
		$color = $data['color'] ? $data['color'] : '#' . self::$svgs[$data['name']]['data']->hex;
		$class = ($data['class']) ? ' ' . $data['class'] : '';

		// build the output
		$output = '<span class="simple-icon-' . $data['name'] . $class . '"';
			$output .= ' style="fill:' . $color . ';';
			$output .= $data['size'] ? ' height:' . $data['size'] . '; width:' . $data['size'] . ';' : '';
			$output .= '">';
				// get the svg, cache it, then display it
				$icon = wp_cache_get( 'simpleicons_icon_' . $data['name'] );
				if ( false === $icon ) {
					$icon = self::get_file(self::$svgs[$data['name']]['data']->svg);
					wp_cache_set( 'simpleicons_icon_' . $data['name'], $icon );
				}
				$output .= $icon;
		$output .= '</span>';
		return $output;
	}

	public static function get_placeholders() {
		return self::$placeholders;
	}

	public static function get_file($file) {
		// avoid ssl errors
		$arrContextOptions=array(
		    "ssl"=>array(
		        "verify_peer"=>false,
		        "verify_peer_name"=>false,
		    ),
		);
		return file_get_contents($file, false, stream_context_create($arrContextOptions));
	}

	private static function slugify_name($name) {
		return str_replace(array(' ', '.'), '', strtolower($name));
	}
}

function simpleicons_register_svg_files() {
	$arrContextOptions=array(
	    "ssl"=>array(
	        "verify_peer"=>false,
	        "verify_peer_name"=>false,
	    ),
	); 

	$data = wp_cache_get( 'simpleicons_json' );
	if ( false === $data ) {
		$url = 'https://cdn.jsdelivr.net/npm/simple-icons@latest/_data/simple-icons.json';
		$json = SimpleIcons::get_file($url);
		$data = json_decode($json);
		wp_cache_set( 'simpleicons_json', $data );
	}

	foreach ($data->icons as $icon) {
		SimpleIcons::register($icon);
	}

} simpleicons_register_svg_files();


function simpleicons_shortcode_func($atts) {
    $a = shortcode_atts( array(
        'name' => null,
        'color' => null,
        'size' => null,
        'class' => null
    ), $atts );
	return SimpleIcons::display($a);  
}
add_shortcode( 'simple_icon', 'simpleicons_shortcode_func' );


// Filters all menu item titles for a #placeholder# 
function simpleicons_menu_items( $menu_items ) {
    // get all potential placeholders
    $placeholders = SimpleIcons::get_placeholders();
    foreach ( $menu_items as $menu_item ) {
    	// check if placeholder exists
        if ( isset($placeholders[$menu_item->title]) ) {
        	// get all shortcodes
            global $shortcode_tags;
            $placeholder = $placeholders[$menu_item->title];
            if ( isset( $shortcode_tags[ $placeholder['shortcode'] ] ) ) {
            	// call the shortcode within the title
                $menu_item->title = call_user_func( 
                    $shortcode_tags[ $placeholder['shortcode'] ], 
                    $placeholder['atts'], 
                    $placeholder['content'], 
                    $placeholder['shortcode']
                );
                // add a simple-icon class to the menu-item
                array_push($menu_item->classes, 'simple-icon');
            }
        }

    }
    return $menu_items;
}
add_filter( 'wp_nav_menu_objects', 'simpleicons_menu_items' );