=== Simple Icons ===
Contributors: tjtaylor
Donate link: https://simpleicons.org/
Tags: social icons, social menu icons, menu icons, social media icons, svg icons
Requires at least: 2.8
Tested up to: 4.9.1
Requires PHP: 5.2.4
Stable tag: 4.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A lightweight svg social icons plugin built on the simpleicons.org framework.


== Contributors ==

[Simple Icon Contributors](https://github.com/simple-icons/simple-icons/graphs/contributors)


== Description ==

This is a social icons plugin built on the [https://simpleicons.org](https://simpleicons.org) framework. Use the shortcode [simple_icon] to display an icon or use #iconname# in your menu item title.


== Notes ==

This plugin uses [http://cdn.jsdelivr.net/](http://cdn.jsdelivr.net/) to serve icons.

Icons are SVG format.

Use [GitHub](https://github.com/simple-icons/simple-icons) for icon requests, corrections and contributions.


== How to use ==

Example:

`[simple_icon name="wordpress" color="purple"]`

Properties:

*	<strong>name</strong>
	The name of the social icon (search [https://simpleicons.org/](https://simpleicons.org) to find an icon).

*	<strong>color</strong> (optional)
	The color of the icon (example: #ffffff or white).
	<em>Default is pulled from the framework</em>

*	<strong>size</strong> (optional)
	The size of the icon
	<em>Default is 1.5rem</em>

*	<strong>class</strong> (optional)
	Add a custom CSS class to the icon.


== Screenshots ==

1. Use simple icons in menu titles
2. Use simple icons as shortcode in your content
3. Highlight shortcode and click on the link button to create icon links
4. Example of front end use.


== Installation ==

This section describes how to install the plugin and get it working.

1. Upload `simple-icons.zip` to the `/wp-content/plugins/` directory
2. Unzip the file
3. Activate the plugin through the 'Plugins' menu in WordPress
4. Use the shortcode [simple_icon] anywhere in your content, or use #example# in your menu item title


== Changelog ==

= 1.0 =
* Initial release

= 1.1 =
* Added json file and icon caching for performance