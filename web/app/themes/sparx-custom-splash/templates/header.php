<header class="banner">
  <div class="row">
    <div class="custom-flex splash-content" style="background: linear-gradient(rgba(60, 255, 255, 0.65), rgba(60, 255, 255, 0.65)), url('<?=get_field('overlay_texture');?>')">
      <h1 class="row justify-content-center">
        <a href="<?=bloginfo('url')?>">
          <?php if(has_site_icon()):?>
          <img src="<?=get_site_icon_url();?>" alt="<?=bloginfo('name')?>">
          <?php endif;?>
          <span <?php if(has_site_icon()):?> class="screen-reader-text" <?php endif;?> ><?=bloginfo('name')?></span>
        </a>
      </h1>
      <h2 class="tag-line row justify-content-center"><p class="half"><?=bloginfo('description')?></p></h2>
      <div class="research row justify-content-center">
        <h3>Read The Research</h3>
       
          <?php if(have_rows('button_content')):?>
          <div class="buttons row justify-content-center align-items-center">
            <?php while(have_rows('button_content')) : the_row();
            $url = get_sub_field('button_source');
            $image = get_sub_field('button_image');?>
            <a href="<?=$url;?>" class="btn"><img src="<?=$image;?>" alt=""></a>
            <?php endwhile;?>
          </div>
          <?php endif;?>
      </div>
    </div>
    <div class="flex main-image" style="background-image:url('<?=get_field('image');?>')"></div>
  </div>
</header>