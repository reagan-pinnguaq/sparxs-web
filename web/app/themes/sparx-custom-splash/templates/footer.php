<?php use Roots\Soil\CustomNav\NavWalker; ?>

<footer class="content-info">
  <div class="container">
  <?php if (has_nav_menu('footer_menu')) :
        wp_nav_menu(['theme_location' => 'footer_menu', 'menu_class' => 'row justify-content-center align-items-end no-wrap py-1', 'walker' => new Custom_Walker_Nav_Menu()]);
      endif; ?>
  </div>
</footer>
