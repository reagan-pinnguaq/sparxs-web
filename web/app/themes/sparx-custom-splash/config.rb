preferred_syntax = :sass
http_path = '/'
css_dir = 'dist/styles/'
sass_dir = 'assets/styles'
images_dir = 'assets/images'
javascripts_dir = 'assets/javascripts'
relative_assets = true
line_comments = true