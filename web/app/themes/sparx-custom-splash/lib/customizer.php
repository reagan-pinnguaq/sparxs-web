<?php

namespace Roots\Sage\Customizer;

use Roots\Sage\Assets;

/**
 * Add postMessage support
 */
function customize_register($wp_customize) {
  $wp_customize->get_setting('blogname')->transport = 'postMessage';
}
add_action('customize_register', __NAMESPACE__ . '\\customize_register');

/**
 * Customizer JS
 */
function customize_preview_js() {
  wp_enqueue_script('sage/customizer', Assets\asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
}
add_action('customize_preview_init', __NAMESPACE__ . '\\customize_preview_js');


/////////// Custom Begins ///////////

/**
*  Disable Comments for posts across site
*/

// Disable support for comments and trackbacks in post types
// function df_disable_comments_post_types_support() {
//  $post_types = get_post_types();
//  foreach ($post_types as $post_type) {
//    if(post_type_supports($post_type, 'comments')) {
//      remove_post_type_support($post_type, 'comments');
//     //  remove_post_type_support($post_type, 'trackbacks');
//    }
//  }
// }
// add_action('admin_init', 'df_disable_comments_post_types_support');

// Close comments on the front-end
// function df_disable_comments_status() {
//  return false;
// }
// add_filter('comments_open', 'df_disable_comments_status', 20, 2);
// add_filter('pings_open', 'df_disable_comments_status', 20, 2);

// // // Hide existing comments
// function df_disable_comments_hide_existing_comments($comments) {
//  $comments = array();
//  return $comments;
// }
// add_filter('comments_array',  __NAMESPACE__ . '\\df_disable_comments_hide_existing_comments', 10, 2);

// // Remove comments page in menu
// function df_disable_comments_admin_menu() {
//  remove_menu_page('edit-comments.php');
// }
// add_action('admin_menu', __NAMESPACE__ . 'df_disable_comments_admin_menu');

// // Redirect any user trying to access comments page
// function df_disable_comments_admin_menu_redirect() {
//  global $pagenow;
//  if ($pagenow === 'edit-comments.php') {
//    wp_redirect(admin_url()); exit;
//  }
// }
// add_action('admin_init', 'df_disable_comments_admin_menu_redirect');

// // Remove comments metabox from dashboard
// function df_disable_comments_dashboard() {
//  remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
// }
// add_action('admin_init', 'df_disable_comments_dashboard');

// // Remove comments links from admin bar
// function my_admin_bar_render() {
//    global $wp_admin_bar;
//    $wp_admin_bar->remove_menu('comments');
// }
// add_action( 'wp_before_admin_bar_render', 'my_admin_bar_render' );