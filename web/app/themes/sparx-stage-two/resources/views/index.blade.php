@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @if (have_posts())
  <section class="news-wrapper">
    <h2>News</h2>
    @while (have_posts()) @php(the_post())
      @include('partials.content-'.get_post_type())
    @endwhile
  <div class="load-more">
    <button id="post" class="btn btn-load-more-posts" id="">Load More</button>
  </div>
  @endif
  </section>

@endsection
