<!doctype html>
<html @php(language_attributes())>
  @include('partials.common.head')
  
  <body @php(body_class())>
    @php(do_action('get_header'))  
    @include('partials.components.skip-to')
    <div id="barba-wrapper" class="master-grid" role="document">
      @include('partials.common.navigation')
      @include('partials.common.header')
            
      <main class="main barba-container">
        @yield('content')
      </main>

      @include('partials.common.background')
      @include('partials.components.feed-partners')
      @include('partials.common.footer')
    </div>
    @php(do_action('get_footer'))
    @php(wp_footer())
  </body>
</html>
