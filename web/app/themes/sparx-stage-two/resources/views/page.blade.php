@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())

    @include('partials.page-header')

    @if(is_page('downloads'))
      @if(is_user_logged_in())
        @include('partials.downloads')
      @else
        @include('partials.components.form-login')
      @endif
    @endif

    @if(is_front_page())
      @include('partials.components.feed-news')
    @endif

    @if(is_page('contact'))
      @include('partials.components.form-contact')
    @endif

  @endwhile
@endsection
