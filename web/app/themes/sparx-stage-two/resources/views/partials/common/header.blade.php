<header class="brand">
  <a href="{{bloginfo('url')}}">
    @if(has_site_icon())
    <img src="{!! \App::image('icon') !!}" alt="{{bloginfo('name')}}">
    @endif
    <span @if(has_site_icon()) class="screen-reader-text" @endif ><{{bloginfo('name')}}</span>
  </a>
</header>