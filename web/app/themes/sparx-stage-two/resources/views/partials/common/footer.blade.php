<footer class="content-info">
  <nav role="footer-navigation">
  {!! wp_nav_menu(['theme_location' => 'footer_navigation', 'container' => false, 'menu_class' => 'nav']) !!}
  </nav>
  <section class="credit">
    <p>&copy; {{date('Y')}} I-SPARX • All Rights Reserved</p>
  </section>
</footer>
