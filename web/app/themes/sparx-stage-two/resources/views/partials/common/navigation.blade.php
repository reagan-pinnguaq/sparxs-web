@if (has_nav_menu('primary_navigation'))
<nav class="nav-direction" >
  <a class="home-show" href="/downloads" alt="Click To Login">
    @if(!is_user_logged_in())
    {{'Login'}}
    @else
    {{'Downloads'}}
    @endif
  </a>
  <a class="home-hidden" href="/">Home</a>
  <section id="mobile-nav">
    <span></span>
    <span></span>
    <span></span>
  </section>
</nav>
<nav id="nav-primary">
  {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'container' => false, 'menu_class' => '', 'walker' => new \App\Walkers\CustomNavWalker ]) !!}
</nav>
@endif
@if (has_nav_menu('soical_menu'))
<nav class="social" role="social-links">
  {!! wp_nav_menu(['theme_location' => 'soical_menu', 'container' => false, 'menu_class' => 'nav']) !!}
</nav>
@endif