<div class="header-background">
<!-- background for nav -->
</div>
@php($image = App::image())
@if ( $image )
<div class="image-background">
  <!-- background for content -->
  @include('partials.components.picture')
</div>
@endif