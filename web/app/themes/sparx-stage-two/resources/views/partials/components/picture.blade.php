<?php use Roots\Sage\Config\Images as Config;?>

<?php if($image && isset($image['url'])):?>

<picture>	
	<?php $i = 0; $config = new Config();?>
	<?php foreach(array_reverse($config->sizes) as $name => $attr):?>
	<?php if(isset($image['sizes'][$name])):?>
		<source media="(min-width: <?=$attr['width']?>px) and (orientation: <?=$attr['orientation'];?>)"  srcset="<?=$image['sizes'][$name];?>">
	<?php endif;?>
	<?php $i++; endforeach;?>

	<img src="<?=$image['url'];?>" alt="<?php echo $image['alt'];?>" >
	
</picture> 
<?php endif;?>