

@if(!is_user_logged_in())
<div class="form-wrapper">
  <header>
      <h2>Login</h2>
      <p>Log-in required to view this content.</p>
  </header>
  {!! wp_login_form( \App\get_login_args()) !!}
  <a href="{{wp_lostpassword_url()}}" class="reset-link">Forgot Your Password?</a>
</div>
@endif

