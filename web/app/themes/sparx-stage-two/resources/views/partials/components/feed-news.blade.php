  @php($posts = \App\get_recent_blog_posts())
  @if($posts->have_posts())
  <section class="feed feed-news feed-preview">
    <h2>News</h2>
    <div class="feed-wrapper">
    <?php while ($posts->have_posts()): $posts->the_post()?>
      @include('partials.content-'.get_post_type().'-preview')
    @endwhile
    </div>
    <div class="button-wrapper">
      <a href="/news" class="btn btn-view-more">View more</a>
    </div>
  </section>
  @endif
  @php(wp_reset_query())
