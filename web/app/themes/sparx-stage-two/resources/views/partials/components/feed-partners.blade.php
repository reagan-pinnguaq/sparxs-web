
<section class="feed feed-partners">
{!! wp_nav_menu(['theme_location' => 'partner_menu', 'container' => false, 'menu_class' => 'nav', 'walker' => new \App\Walkers\CustomNavWalker()]) !!}
</section>