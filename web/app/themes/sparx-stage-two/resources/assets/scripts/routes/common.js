export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    // Do barba stuff
    document.addEventListener("DOMContentLoaded", () => {
      Barba.Pjax.start();
    });

    //mobile nav toggle
    $("#mobile-nav").click(function() {
      $(this).toggleClass("is-active");
      $("nav#nav-primary").toggleClass("on");
    });

    //Ajax posts

    var postOffset = 1;

    function increment() {
      return postOffset++;
    }

    function get_offset() {
      return postOffset;
    }

    $(".btn-load-more-posts")
      .unbind("click")
      .bind("click", function(e) {
        e.preventDefault();
        var offset = get_offset();
        var container = $(".news-wrapper");
        var category = $(container).attr("data-category");
        $.ajax({
          url: enqueData.ajax_url,
          type: "POST",
          data: {
            action: "blogPostFetch",
            offset: offset,
            category: category,
          },
          success: function(response) {
            if (response && response !== "false") {
              $(container).append(response);
              increment();
            } else {
              $(this)
                .parent()
                .remove();
              $(container).append(
                '<p class="notice">Sorry, we ran out of content</p>'
              );
            }
          },
        });
      });
  },
};
