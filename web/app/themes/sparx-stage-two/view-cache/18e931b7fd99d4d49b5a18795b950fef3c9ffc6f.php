

<?php $__env->startSection('content'); ?>
  <?php while(have_posts()): ?> <?php (the_post()); ?>

    <?php echo $__env->make('partials.page-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php if(is_page('downloads')): ?>
      <?php if(is_user_logged_in()): ?>
        <?php echo $__env->make('partials.downloads', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php else: ?>
        <?php echo $__env->make('partials.components.form-login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php endif; ?>
    <?php endif; ?>

    <?php if(is_front_page()): ?>
      <?php echo $__env->make('partials.components.feed-news', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    <?php if(is_page('contact')): ?>
      <?php echo $__env->make('partials.components.form-contact', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

  <?php endwhile; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>