<!doctype html>
<html <?php (language_attributes()); ?>>
  <?php echo $__env->make('partials.common.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  
  <body <?php (body_class()); ?>>
    <?php (do_action('get_header')); ?>  
    <?php echo $__env->make('partials.components.skip-to', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div id="barba-wrapper" class="master-grid" role="document">
      <?php echo $__env->make('partials.common.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->make('partials.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            
      <main class="main barba-container">
        <?php echo $__env->yieldContent('content'); ?>
      </main>

      <?php echo $__env->make('partials.common.background', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->make('partials.components.feed-partners', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->make('partials.common.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
    <?php (do_action('get_footer')); ?>
    <?php (wp_footer()); ?>
  </body>
</html>
