<footer class="content-info">
  <nav role="footer-navigation">
  <?php echo wp_nav_menu(['theme_location' => 'footer_navigation', 'container' => false, 'menu_class' => 'nav']); ?>

  </nav>
</footer>
