<?php if(has_nav_menu('primary_navigation')): ?>
<nav class="nav-primary" id="nav-primary" role="navigation">
  <a href="<?php echo e(wp_login_url( $redirect )); ?>" alt="Click To Login">Login</a>

  <section id="mobile-nav">
    <span></span>
    <span></span>
    <span></span>
  </section>

  <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'container' => false, 'menu_class' => 'nav']); ?>

</nav>
<?php endif; ?>
<?php if(has_nav_menu('social')): ?>
<nav class="social" role="social-links">
  <?php echo wp_nav_menu(['theme_location' => 'soical_menu', 'container' => false, 'menu_class' => 'nav']); ?>

</nav>
<?php endif; ?>