<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Tell WordPress how to find the compiled path of comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );
    return template_path(locate_template(["views/{$comments_template}", $comments_template]) ?: $comments_template);
});

/**
 * Redo the media Uploader to make them picture elements
 */
/**
 * Source size generator
 *
 * @param int $image (attachment_id)
 * @return void
 */
function pinnguaq_get_picture_srcs( $image) {
    $mappings = \App\get_image_sizes();
    $arr = array();
    foreach ( $mappings as $type => $size  ) {
        $image_src = wp_get_attachment_image_src( $image, $type );
        $arr[] = '<source srcset="'. $image_src[0] . '" media="(min-width: '. $size['width'] .'px)">';
    }
    return implode( array_reverse ( $arr ) );
}

/**
 * Overide function for responseive image inset using media tool
 */
function responsive_insert_image($html, $id, $caption, $title, $align, $url) {
    return  '<picture class="align-'. $align . '">
    <!--[if IE 9]><video style="display: none;"><![endif]-->'
    . pinnguaq_get_picture_srcs($id) .
    '<!--[if IE 9]></video><![endif]-->'.
    '<img src="' . wp_get_attachment_image_src($id, 'full')[0] . '" alt="' . $id .'">
</picture>';
  }
  add_filter('image_send_to_editor', __NAMESPACE__.'\\responsive_insert_image', 10, 9);

add_filter('wpcf7_form_elements', function($content) {
    $content = str_ireplace('<p>','',$content);
    $content = str_ireplace('</p>','',$content);
    // $content = preg_replace('/<p\b[^>]*>(.*?)<\/p>/i', '', $content);
    return $content;
});

/**
 * Tell Wordpress to allow svgs and add filter for editor
 */
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter('upload_mimes', __NAMESPACE__ .'\cc_mime_types');
  function fix_svg_thumb_display() {
    echo '
    <style>
      td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail { 
        width: 100% !important; 
        height: auto !important; 
      }
      </style>
    ';
  }
add_action('admin_head', __NAMESPACE__ .'\fix_svg_thumb_display');

//Tribe Template Setting filter
if(class_exists('Tribe__Settings_Manager')){
    \Tribe__Settings_Manager::set_option( 'tribeEventsTemplate', 'views/events.blade.php' );
}


// Send non admins to the downloads section of the site
function redirect_admin( $redirect_to, $request, $user ){

    if ( isset( $user->roles ) && is_array( $user->roles ) ) {
        //check for admins
        if ( in_array( 'administrator', $user->roles ) || in_array( 'editor', $user->roles ) ) {
            return $redirect_to;
        }
        $redirect_to = WP_HOME.'/downloads/';
    }
    return $redirect_to;
}

add_filter( 'login_redirect',__NAMESPACE__ .'\redirect_admin', 10, 3 );

//remove spans from contact form 7
add_filter('wpcf7_form_elements', function($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
    // $content = preg_replace('/<(div).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
    return $content;
});

function wpdocs_custom_excerpt_length( $length ) {
    return 25;
}
add_filter( 'excerpt_length',__NAMESPACE__ .'\wpdocs_custom_excerpt_length', 999 );

