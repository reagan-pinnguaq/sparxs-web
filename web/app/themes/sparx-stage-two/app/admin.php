<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});

/**
 * AJAX
 */
add_action('wp_ajax_blogPostFetch', __NAMESPACE__ . '\\blogPostFetch');
add_action('wp_ajax_nopriv_blogPostFetch', __NAMESPACE__ . '\\blogPostFetch');

/**
 * fetches most recent blog posts
 *
 * @return void
 */
function blogPostFetch(){
    if($_POST['offset']){
        
        $display_count = get_option( 'posts_per_page' );
        $offset = $_POST['offset'];
        $cat = (isset($_POST['category']) && $_POST['category'] !== 'undefined') ? $_POST['category'] : false;

        $args = array(
            'posts_per_page'	=> $display_count,
            'offset' => $offset * $display_count,
            'post_type' => 'post'
        );
        if($cat){
            $args['category_name'] = $cat;
        }
        
        $the_query = new \WP_Query( $args );

        if( $the_query->have_posts() ):
            while( $the_query->have_posts() ) : $the_query->the_post();
            echo \App\template(locate_template('views/partials/content.blade.php'));  
            endwhile;
        else :
            echo 'false';
        endif;
        // echo '<p>' . $_POST['offset'] .'</p>';
    }
	die();
}