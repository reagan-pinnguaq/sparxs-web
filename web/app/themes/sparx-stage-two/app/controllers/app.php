<?php

namespace App;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title() {
        if (is_home()) {
          if (get_option('page_for_posts', true)) {
            $archive_title = get_field('page_description', get_page_by_title(get_the_title(get_option('page_for_posts', true))));
            return ($archive_title) ? $archive_title :  get_the_title(get_option('page_for_posts', true));
          } else {
            return __('Latest Posts', 'sage');
          }
        }
          if (is_archive()) {
            $archive_title = get_field('page_description', get_page_by_title(get_the_archive_title()));
            return ($archive_title) ? $archive_title : get_the_archive_title();
        }
        if(is_page('downloads')){
            return (is_user_logged_in()) ? get_the_title() : __('“The overarching goal for I-SPARX is to combine an evidence-informed intervention for preventing depression and suicide with empowered cultural practices that have historically supported resilience.” (I-SPARX Project Proposal submitted to CIHR)', 'sage');
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        if(is_page()){
            return get_field('page_description');
        }
        return get_the_title();
      }
    // }

      public static function tagline(){
        return get_field('page_description');
      }
      
      /**
       * Image fetcher
       *
       * @param boolean $id
       * @param boolean $page
       * @return void
       */
      public static function image($id = false, $page = false){
        if(!$id){
          $id =  'image';
        } elseif ($id == 'icon' || $id == 'site-icon'){
          if(has_site_icon()){
              return get_site_icon_url();
          }
        }      
        if (is_home() || is_archive() || is_404() || is_search() || is_singular()) {
          return get_field($id, get_option('page_for_posts'));
        }

        return get_field($id);
       
      }

    /**
     * Navigation menu's
     */

    public static function partners_menu(){
        return ['theme_location' => 'partner_menu', 'container' => false, 'menu_class' => 'row justify-content-center align-items-end no-wrap py-1', 'walker' => new \Custom_Walker_Nav_Menu()];
    }
    
    public static function social_menu(){
        return ['theme_location' => 'soical_menu', 'container' => false, 'menu_id' => 'social-icons', 'walker' => new \Custom_Walker_Nav_Menu()];
    }
    
    public static function main_menu(){
        return ['theme_location' => 'primary_navigation', 'container' => false, 'menu_id' => 'Primary Navigation'];
    }

}
