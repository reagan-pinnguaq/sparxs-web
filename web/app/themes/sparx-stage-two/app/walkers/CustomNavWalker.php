<?php
namespace App\Walkers;

class CustomNavWalker extends \Walker_Nav_Menu {
    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= '<ul class="row justify-content-center">';
    }
  
    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= '</ul>';
    }
  
    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $classes = array();
        if( !empty( $item->classes ) ) {
            $classes = (array) $item->classes;
        }
        $class_list = '';
        foreach($classes as $class){
            $class_list .= $class . ' ';
        }
        $active_class = '';
        if( in_array('current-menu-item', $classes) ) {
            $active_class = 'active';
        } else if( in_array('current-menu-parent', $classes) ) {
            $active_class = 'active-parent';
        } else if( in_array('current-menu-ancestor', $classes) ) {
            $active_class = 'active-ancestor';
        }
        $class_list .= $active_class;
          
        $url = '';
        if( !empty( $item->url ) ) {
            $url = $item->url;
        }
        if(get_field('icon', $item)){
            $output .= '<li class="'.  $class_list . '"><a href="' . $url . '"><img src="'. get_field('icon', $item) .'" alt='. $item->title .'"></a></li>'; 
        } else {
            $output .= '<li class="'.  $class_list . '"><a href="' . $url . '">' . $item->title . '</a></li>';
        }
        
    }
  
    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
        $output .= '</li>';
    }
  }
  
  