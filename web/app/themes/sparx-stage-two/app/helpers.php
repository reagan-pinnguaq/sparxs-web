<?php

namespace App;

use Roots\Sage\Container;

/**
 * Get the sage container.
 *
 * @param string $abstract
 * @param array  $parameters
 * @param Container $container
 * @return Container|mixed
 */
function sage($abstract = null, $parameters = [], Container $container = null)
{
    $container = $container ?: Container::getInstance();
    if (!$abstract) {
        return $container;
    }
    return $container->bound($abstract)
        ? $container->makeWith($abstract, $parameters)
        : $container->makeWith("sage.{$abstract}", $parameters);
}

/**
 * Get / set the specified configuration value.
 *
 * If an array is passed as the key, we will assume you want to set an array of values.
 *
 * @param array|string $key
 * @param mixed $default
 * @return mixed|\Roots\Sage\Config
 * @copyright Taylor Otwell
 * @link https://github.com/laravel/framework/blob/c0970285/src/Illuminate/Foundation/helpers.php#L254-L265
 */
function config($key = null, $default = null)
{
    if (is_null($key)) {
        return sage('config');
    }
    if (is_array($key)) {
        return sage('config')->set($key);
    }
    return sage('config')->get($key, $default);
}

/**
 * @param string $file
 * @param array $data
 * @return string
 */
function template($file, $data = [])
{
    if (remove_action('wp_head', 'wp_enqueue_scripts', 1)) {
        wp_enqueue_scripts();
    }

    return sage('blade')->render($file, $data);
}

/**
 * Retrieve path to a compiled blade view
 * @param $file
 * @param array $data
 * @return string
 */
function template_path($file, $data = [])
{
    return sage('blade')->compiledPath($file, $data);
}

/**
 * @param $asset
 * @return string
 */
function asset_path($asset)
{
    return sage('assets')->getUri($asset);
}

/**
 * @param string|string[] $templates Possible template files
 * @return array
 */
function filter_templates($templates)
{
    $paths = apply_filters('sage/filter_templates/paths', [
        'views',
        'resources/views'
    ]);
    $paths_pattern = "#^(" . implode('|', $paths) . ")/#";

    return collect($templates)
        ->map(function ($template) use ($paths_pattern) {
            /** Remove .blade.php/.blade/.php from template names */
            $template = preg_replace('#\.(blade\.?)?(php)?$#', '', ltrim($template));

            /** Remove partial $paths from the beginning of template names */
            if (strpos($template, '/')) {
                $template = preg_replace($paths_pattern, '', $template);
            }

            return $template;
        })
        ->flatMap(function ($template) use ($paths) {
            return collect($paths)
                ->flatMap(function ($path) use ($template) {
                    return [
                        "{$path}/{$template}.blade.php",
                        "{$path}/{$template}.php",
                        "{$template}.blade.php",
                        "{$template}.php",
                    ];
                });
        })
        ->filter()
        ->unique()
        ->all();
}

/**
 * @param string|string[] $templates Relative path to possible template files
 * @return string Location of the template
 */
function locate_template($templates)
{
    return \locate_template(filter_templates($templates));
}

/**
 * Determine whether to show the sidebar
 * @return bool
 */
function display_sidebar()
{
    static $display;
    isset($display) || $display = apply_filters('sage/display_sidebar', false);
    return $display;
}


/**
 * Debug
 */
function pr($i, $j = null){
	echo "<pre>"; var_dump($i); echo "</pre>";
	if($j != null) {
		die();
	}
}

/*
 * String functions
 */
function limit_text($text, $limit) {
	if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '... <a href="'. get_the_permalink() .'">Read More</a>' ;
        $text = closetags($text);
	}
	return $text;
}

function closetags($html) {
	preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
	$openedtags = $result[1];
	preg_match_all('#</([a-z]+)>#iU', $html, $result);
	$closedtags = $result[1];
	$len_opened = count($openedtags);
	if (count($closedtags) == $len_opened) {
			return $html;
	}
	$openedtags = array_reverse($openedtags);
	for ($i=0; $i < $len_opened; $i++) {
			if (!in_array($openedtags[$i], $closedtags)) {
					$html .= '</'.$openedtags[$i].'>';
			} else {
					unset($closedtags[array_search($openedtags[$i], $closedtags)]);
			}
	}
	return $html;
} 


function linkify($link, $id, $tag = ''){
	return '<a href="' . $link . $id .'">'  . $tag . $id .'</a>';
}

/**
 * Data helpers
 */

 /**
 * WP Object Fetches
 */
function get_page_by_name($page){
	$args = array('post_type' => 'page', 'pagename' => $page); 
	$result = new \WP_Query( $args );
	return $result;
}

function get_archive_by_type($post_type){
	$args = array('post_type' => $post_type, 'status' => 'published');
	$result = new \WP_Query( $args );
	return $result;
}

function get_recent_blog_posts($numb = false){
  $numb = ($numb) ? $numb : 2;
	$args = array('posts_per_page' => $numb, 'post_status' => 'publish', 'post_type' => 'post');
	$result = new \WP_Query( $args );
	return $result;
}

function get_archive_by_type_orderby_custom_field($post_type, $order, $direction, $is_numb = false, $limit = false){
	if($is_numb == true){
		$meta = "meta_value_num";
	} else {
		$meta = "meta_value";
	}
	if($limit == false){
		$limit = -1;
	}
		$args = array(
		'post_type' 		=> $post_type,
		'posts_per_page'	=> $limit,
		'meta_key'			=> $order,
		'orderby'			=> $meta,
		'order'				=> $direction
	);
	$result = new \WP_Query( $args );
	return $result;
}

function get_archive_by_type_where_value($post_type, $key, $value, $limit = false){
	if($limit == false){
		$limit = -1;
	}
	$args = array(
		'post_type'		=> $post_type,
		'numberposts'	=> $limit,
		'meta_key'		=> $key,
		'meta_value'	=> $value
	);
	$result = new \WP_Query( $args );
	return $result;
}

function get_image_sizes() {
	global $_wp_additional_image_sizes;

	$sizes = array();

	foreach ( get_intermediate_image_sizes() as $_size ) {
		if ( in_array( $_size, array('thumbnail', 'medium', 'medium_large', 'large') ) ) {
			$sizes[ $_size ]['width']  = get_option( "{$_size}_size_w" );
			$sizes[ $_size ]['height'] = get_option( "{$_size}_size_h" );
			$sizes[ $_size ]['crop']   = (bool) get_option( "{$_size}_crop" );
		} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
			$sizes[ $_size ] = array(
				'width'  => $_wp_additional_image_sizes[ $_size ]['width'],
				'height' => $_wp_additional_image_sizes[ $_size ]['height'],
				'crop'   => $_wp_additional_image_sizes[ $_size ]['crop'],
			);
		}
	}

	return $sizes;
}

function get_login_args(){
	   $args = array(
        'redirect' => admin_url(), 
        'form_id' => 'loginform-custom',
        'label_username' => __( 'Username' ),
        'label_password' => __( 'Password' ),
        'label_remember' => __( 'Remember Me' ),
        'label_log_in' => __( 'Log In' ),
        'remember' => true
    );
    return $args;
}