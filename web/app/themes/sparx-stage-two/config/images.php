<?php 

namespace Roots\Sage\Config;

class Images {

  public $sizes = array();

  public function __construct(){
    $sizes = [
    "mobile"                 => array( 'orientation' => 'portrait', 'width' => 300, 'height' => 500),
    "tablet-portait"         => array( 'orientation' => 'portrait', 'width' => 500, 'height' => 800 ),
    "small-desktop"          => array( 'orientation' => 'landscape', 'width' => 1100, 'height' => 700),
    "large-desktop"          => array( 'orientation' => 'landscape', 'width' => 1500, 'height' => 950)
    ]; 
    $this->sizes = $sizes;
    return $this->sizes;
  }
}